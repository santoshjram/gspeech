/****************************************************************
* Author : Santosh kumar
* 31/03/2016
* Handling all DB related methods
*****************************************************************/

package speechdb

import (
	"database/sql"
	"encoding/json"
	_ "github.com/go-sql-driver/mysql"
	. "global"
	"log"
	"os"
)

var DBSettings struct {
	Username string `json:"userName"`
	Password string `json:"passWord"`
	DB       string `json:"db"`
	Host     string `json:"host"`
}

//Load DB settings from db.json
func init() {
	configFile, err := os.Open("db.json")
	if err != nil {
		log.Println("db.json file error")
	}
	jsonParser := json.NewDecoder(configFile)
	if err = jsonParser.Decode(&DBSettings); err != nil {
		log.Println("parsing db.json config file", err.Error())
	}
	log.Println(DBSettings.Username)
}

//Initialise Global data structure with speech table
func Loaddata(GlobalData map[string]GlobalDS) {
	con, err := sql.Open("mysql", DBSettings.Username+":"+DBSettings.Password+"@tcp("+DBSettings.Host+":3306)/"+DBSettings.DB)
	if err != nil {
		log.Println(err)
	}
	rows, err := con.Query("SELECT * FROM speech")
	if err == nil {
		for rows.Next() {
			var sparkid string
			var mobile string
			var search string
			err = rows.Scan(&sparkid, &mobile, &search)
			if err == nil {
				GlobalData[search] = GlobalDS{append(GlobalData[search].Mobile, mobile), append(GlobalData[sparkid].Sparkid, sparkid)}
				log.Println(sparkid)
				log.Println(mobile)
				log.Println(search)
			} else {
				log.Println(err)
			}
		}
	}
	defer con.Close()
}

//To verify whether sparkID already exist
func Verifysparkid(sparkid string) (search string, mobile string) {
	con, err := sql.Open("mysql", DBSettings.Username+":"+DBSettings.Password+"@tcp("+DBSettings.Host+":3306)/"+DBSettings.DB)
	if err != nil {
		log.Println(err)
	}
	rows, err := con.Query("SELECT search,mobile FROM speech where sparkid=?", sparkid)
	if err == nil {
		for rows.Next() {
			var search string
			var mobile string
			err = rows.Scan(&search, &mobile)
			if err == nil {
				log.Println("Verifysparkid:-", search)
				return search, mobile
			}
		}
	}
	log.Println("Verifysparkid:-", sparkid)
	return "", ""

}

//To verify whether search phrase already exist
func Verifysearch(search string) (value string) {
	con, err := sql.Open("mysql", DBSettings.Username+":"+DBSettings.Password+"@tcp("+DBSettings.Host+":3306)/"+DBSettings.DB)
	if err != nil {
		log.Println(err)
	}
	rows, err := con.Query("SELECT search FROM speech where search=?", search)
	if err == nil {
		for rows.Next() {
			var search string
			err = rows.Scan(&search)
			if err == nil {
				log.Println("Verifysearch:-", search)
				return search
			}
		}
	}
	log.Println("Verifysearch:-", search)
	return ""

}

//Update search phrase
func Updatesearch(sparkid string, search string) (status bool) {
	log.Println("In UPDATE SEARCH", sparkid, search)
	con, err := sql.Open("mysql", DBSettings.Username+":"+DBSettings.Password+"@tcp("+DBSettings.Host+":3306)/"+DBSettings.DB)
	if err != nil {
		log.Println(err)
	}
	stinsert, err := con.Prepare("UPDATE speech SET search=? WHERE sparkid=?")
	if err == nil {
		stinsert.Exec(search, sparkid)
		return true
	} else {
		log.Println(err)
	}
	defer con.Close()
	return false

}

//New spark/search
func Storerecord(sparkid string, mobile string, search string) (status bool) {
	log.Println("In Storerecord", sparkid, mobile, search)
	con, err := sql.Open("mysql", DBSettings.Username+":"+DBSettings.Password+"@tcp("+DBSettings.Host+":3306)/"+DBSettings.DB)
	if err != nil {
		log.Println(err)
		log.Println(err)
	}
	stinsert, err := con.Prepare("INSERT speech SET sparkid=?,mobile=?,search=?")
	if err == nil {
		stinsert.Exec(sparkid, mobile, search)
		return true
	} else {
		log.Println(err)
	}
	defer con.Close()
	return false
}
