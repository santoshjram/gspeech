/****************************************************************
* Author : Santosh kumar
* 31/03/2016
* Demo application
*****************************************************************/

package main

import (
	"fmt"
	. "global"
	. "gspeechimpl"
	"html/template"
	"io"
	"log"
	"net/http"
	"os"
	"os/exec"
	. "spark"
	. "speechdb"
	"strings"
)

type templatedata struct {
	Sparkid string
	Mobile  string
	Search  string
}

/*Load user search phrases and sparkid from database*/
func init() {
	log.Println("Initialising in App")
	//GlobalLock.Lock()
	Loaddata(GlobalData)
	log.Println("Initialising in App:- ", GlobalData["testing"].Sparkid)
	//GlobalLock.Unlock()
}

/*Landing page */
func handler(w http.ResponseWriter, r *http.Request) {
	t, _ := template.ParseFiles("f1.html")
	t.Execute(w, nil)
}

/*Update Search phrase */
func updatematch(w http.ResponseWriter, r *http.Request) {
	log.Println("Update match to be added", r.FormValue("sparkid"))
	//Verify search phrase is unique
	if Verifysearch(r.FormValue("search")) == "" {
		returnvalue := Updatesearch(r.FormValue("sparkid"), r.FormValue("search"))
		if returnvalue == true {
			GlobalData[strings.ToLower(r.FormValue("search"))] = GlobalDS{append(GlobalData[r.FormValue("search")].Mobile, r.FormValue("mobile")), append(GlobalData[r.FormValue("search")].Sparkid, r.FormValue("sparkid"))}
			t, _ := template.ParseFiles("f3.html")
			t.Execute(w, nil)
		} else {
			t, _ := template.ParseFiles("error.html")
			t.Execute(w, nil)
		}
		log.Println("selected room to be added", r.FormValue("search"))
	} else {
		//Duplicate search phrase found
		t, _ := template.ParseFiles("duplicatesearch.html")
		t.Execute(w, nil)
	}
}

/*Add new search phrase*/
func addmatch(w http.ResponseWriter, r *http.Request) {
	log.Println("NEW:- addmatch to be added", r.FormValue("search"))
	log.Println("NEW:- mobilenumber to be added", r.FormValue("Mobile"))

	if Verifysearch(r.FormValue("search")) == "" {
		returnvalue := Storerecord(r.FormValue("sparkid"), r.FormValue("mobile"), r.FormValue("search"))
		if returnvalue == true {
			GlobalData[strings.ToLower(r.FormValue("search"))] = GlobalDS{append(GlobalData[r.FormValue("search")].Mobile, r.FormValue("mobile")), append(GlobalData[r.FormValue("search")].Sparkid, r.FormValue("sparkid"))}
			t, _ := template.ParseFiles("f3.html")
			t.Execute(w, nil)
		} else {
			t, _ := template.ParseFiles("error.html")
			t.Execute(w, nil)
		}
		log.Println("selected room to be added", r.FormValue("search"))
	} else {
		t, _ := template.ParseFiles("duplicatesearch.html")
		t.Execute(w, nil)
	}

}

/*Handler function for receiving recorded file from Tropo*/
func Uploadfile(w http.ResponseWriter, r *http.Request) {
	file, handler, err := r.FormFile("filename")
	if err != nil {
		log.Println(err)
		return
	}
	defer file.Close()
	fmt.Fprintf(w, "%v", handler.Header)
	log.Println("Saving file %s", handler.Filename)
	f, err := os.OpenFile("/root/gspeech/src/wav/"+handler.Filename, os.O_WRONLY|os.O_CREATE, 0666)
	if err != nil {
		log.Println(err)
		return
	}
	defer f.Close()
	io.Copy(f, file)
	//Convert wav to flac format
	SoxCmd := exec.Command("sox", "wav/"+handler.Filename, "wav/"+handler.Filename+".flac")

	SoxOut, err := SoxCmd.Output()
	if err != nil {
		panic(err)
	}
	log.Println("Processed file to flac")
	log.Println(string(SoxOut))
	//Processflac handles the Speech to text
	output := Processflac("wav/" + handler.Filename + ".flac")
	log.Println("Processed string ", output)
	//Verify search string matches
	tospark := GlobalData[strings.ToLower(output)].Sparkid
	if tospark != nil {
		log.Println("To Spark", tospark, output)
		//Send spark message
		Sendspark("Message is "+output, tospark[0])
	}
}

//Spark ID addition
func sparkadd(w http.ResponseWriter, r *http.Request) {
	log.Println("SparkID to be added", r.FormValue("sparkid"))
	log.Println("Mobile to be added", r.FormValue("mobile"))
	//Verify SparkID already exist
	search, mobile := Verifysparkid(r.FormValue("sparkid"))
	var A1 templatedata
	if search != "" {
		log.Println("sparkadd:- mobile, form", mobile, r.FormValue("mobile"))
		//SparkID already exist - Verify mobile number matches - security check
		if mobile != r.FormValue("mobile") {
			t, _ := template.ParseFiles("invalidmobile.html")
			t.Execute(w, A1)
		} else {
			t, _ := template.ParseFiles("f2.html")
			A1.Sparkid = r.FormValue("sparkid")
			A1.Mobile = r.FormValue("mobile")
			A1.Search = search
			t.Execute(w, A1)
		}
	} else {
		//New SparkID to be added
		t, _ := template.ParseFiles("f2a.html")
		A1.Sparkid = r.FormValue("sparkid")
		A1.Mobile = r.FormValue("mobile")
		A1.Search = "1234"
		t.Execute(w, A1)
	}
	log.Println("selected sparkid to be added", r.FormValue("sparkid"))
}

func main() {
	/*Main handler for index*/
	http.HandleFunc("/", handler)
	/*SparkID handler*/
	http.HandleFunc("/addsparkid", sparkadd)
	/*New search phrase update*/
	http.HandleFunc("/addmatch", addmatch)
	/*Update search phrase*/
	http.HandleFunc("/updatematch", updatematch)
	/*Recording file from Tropo*/
	http.HandleFunc("/uploadmediaf", Uploadfile)

	http.ListenAndServe(":8802", nil)
}
