/****************************************************************
* Author : Santosh kumar
* 31/03/2016
*****************************************************************/

package global

var GlobalData = make(map[string]GlobalDS)

//var GlobalLock sync.Mutex

type GlobalDS struct {
	Mobile  []string
	Sparkid []string
}
