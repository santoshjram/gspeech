/****************************************************************
* Author : Santosh kumar
* 31/03/2016
* For sending Spark message
*****************************************************************/

package spark

import (
	"bytes"
	"encoding/json"
	"log"
	"net/http"
	"os"
)

var Authtoken string

var Sparksettings struct {
	Authtoken string `json:"Authtoken"`
}

//Load Token from auth.json
func init() {
	configFile, err := os.Open("auth.json")
	if err != nil {
		log.Println("auth.json file error")
	}
	jsonParser := json.NewDecoder(configFile)
	if err = jsonParser.Decode(&Sparksettings); err != nil {
		log.Println("parsing auth.json config file", err.Error())
	}
	Authtoken = Sparksettings.Authtoken
	log.Println(Authtoken)
}

//For sending 1:1 spark message
func Sendspark(message string, personid string) {
	url := "https://api.ciscospark.com/v1/messages"
	var jsonStr = []byte(`{"toPersonEmail":"` + personid + `","text":"` + message + `"}`)
	log.Println("json ", string(jsonStr))

	req, err := http.NewRequest("POST", url, bytes.NewBuffer(jsonStr))
	req.Header.Set("Authorization", "Bearer "+Authtoken)
	req.Header.Set("Content-Type", "application/json")

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		panic(err)
	}
	defer resp.Body.Close()

	log.Println("response Status:", resp.Status)
	log.Println("response Headers:", resp.Header)

}
