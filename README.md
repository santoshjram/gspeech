# README #

![Slide1.png](https://bitbucket.org/repo/78k4BE/images/3145752132-Slide1.png)

# Quick summary #
Tropo integration with Google cloud API. Repo consist of
* gSpeech library for handling full duplex communication
* GUI
* Simple Spark interface for messaging

# Version #
version 0.1


# Configuration #

## auth.json (Spark auth token) ##
{
    "Authtoken": "Spark Auth token here"
}

## db.json (database) ##
{
    "userName": "root",
    "passWord": "your Password",
    "db": "speechdb",
    "host": "127.0.0.1"
}

## google.json (Speech api key) ##
{
    "Key": "your Google Key"
}


# Database configuration #

create database speechdb;

CREATE TABLE speech (
    sparkid VARCHAR(1024) NOT NULL, 
    mobile VARCHAR(100) NOT NULL,  
    search VARCHAR(1024) NOT NULL,  
    PRIMARY KEY (sparkid, mobile,search)
);

* Dependencies
mysql library


# Instructions #
demo :- https://youtu.be/yfB7Mvi7F2U

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact